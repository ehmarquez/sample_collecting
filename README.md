# Sample Collecting for Test Runs #

### What is this repository for? ###

This repo containts the error\_collecting.py script to collect the test values during run time.
The main file can be found in sample\_collecting/src/error\_collecting.py.

It can be run as a simple python script or using

	rosrun sample_collecting error_collecting.py

### Contact Info ###

Contact: alvinmarquez7@gmail.com