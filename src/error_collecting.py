#!/usr/bin/env python
import rospy
from sensor_msgs.msg import Imu, MagneticField
from geometry_msgs.msg import PoseWithCovarianceStamped
from nav_msgs.msg import Odometry
import csv
import os
import errno
import sys

'''
6/5 - added timestamping to DIRNAME

appends timestamp to front of folder name
'''
from datetime import datetime

timestamp = datetime.now()
year = str(timestamp.year)
month = str(timestamp.month)
day = str(timestamp.day)
hour = str(timestamp.hour)
minute = str(timestamp.minute)

# Create csv files and OVERWRITE old files if it already exists!
DIRNAME = '{}_{}-{}_{}-{}_imu_only_test'.format(year, month, day, hour, minute)
FNAME0 = 'log_raw_imu.csv'
FNAME1 = 'log_imu.csv'
FNAME2 = 'log_ekf.csv'
FNAME3 = 'log_disp.csv'
FNAME4 = 'log_gps.csv'
FNAME5 = 'log_mag.csv'

# Possibly make it so that it will append an increasing number if folder name
# already exists (ex. dirname > dirname2 > dirname3 > ..... > and so on)
if os.path.exists(DIRNAME):
    print('Directory {} exists! Please choose a new name before continuing').\
    format(DIRNAME)
    sys.exit()
else:
    # Initially create the log files and overwrite them if it already exists
    # This makes sure that it starts off blank and not appending to an old file
    try:
        os.makedirs(DIRNAME)
    except OSError as exc:          # Guard against race condition according to StackOverflow
        if exc.errno != errno.EEXIST:
            raise

    with open(DIRNAME + '/' + FNAME0, 'wb') as f:
        print 'creating log file for RAW accelerometer...'

    with open(DIRNAME + '/' + FNAME1, 'wb') as f:
        print 'creating log file for accelerometer...'

    with open(DIRNAME + '/' + FNAME2, 'wb') as f:
        print 'Creating log file for ekf output...'

    with open(DIRNAME + '/' + FNAME3, 'wb') as f:
        print 'creating log file for vo output...'

    with open(DIRNAME + '/' + FNAME4, 'wb') as f:
        print 'creating log file for gps output...'

    with open(DIRNAME + '/' + FNAME5, 'wb') as f:
        print 'creating log file for mag output...'

def log_raw_imu(imu):
    '''
    Callback function for imu_bbb topic.
    The csv file format heading is as follows:
    'Timestamp, linear_acceleration_x, linear_acceleration_y, linear_acceleration_z, angular_vel_x, angular_vel_y, angular_vel_z, quaternion_x, quaternion_y, quaternion_z, quaternion_w'
    '''

    print 'Listening to imu topic...'

    # Collect values from IMU
    imu_msg = Imu()
    imu_msg = imu
    imu_time = imu_msg.header.stamp.secs

    # Quaternion values (absolute position)
    quat_x = imu.orientation.x
    quat_y = imu.orientation.y
    quat_z = imu.orientation.z
    quat_w = imu.orientation.w

    # Angular velocity
    ang_x = imu.angular_velocity.x
    ang_y = imu.angular_velocity.y
    ang_z = imu.angular_velocity.z

    # Linear Acceleration values
    lin_acc_x = imu.linear_acceleration.x
    lin_acc_y = imu.linear_acceleration.y
    lin_acc_z = imu.linear_acceleration.z

    with open(DIRNAME + '/' + FNAME0, 'a') as f:
        writer = csv.writer(f)
        writer.writerow([imu_time, lin_acc_x, lin_acc_y, lin_acc_z, ang_x, \
        ang_y, ang_z, quat_x, quat_y, quat_z, quat_w])
        print("Just added IMU output!")

    #rospy.loginfo(imu_msg)

def log_imu(imu):
    '''
    Callback function for imu_data topic.
    The csv file format heading is as follows:
    'Timestamp, linear_acceleration_x, linear_acceleration_y, linear_acceleration_z, angular_vel_x, angular_vel_y, angular_vel_z, quaternion_x, quaternion_y, quaternion_z, quaternion_w'
    '''
    print 'Listening to imu topic...'

    # Collect values from IMU
    imu_msg = Imu()
    imu_msg = imu
    imu_time = imu_msg.header.stamp.secs

    # Quaternion values (absolute position)
    quat_x = imu.orientation.x
    quat_y = imu.orientation.y
    quat_z = imu.orientation.z
    quat_w = imu.orientation.w

    # Angular velocity
    ang_x = imu.angular_velocity.x
    ang_y = imu.angular_velocity.y
    ang_z = imu.angular_velocity.z

    # Linear Acceleration values
    lin_acc_x = imu.linear_acceleration.x
    lin_acc_y = imu.linear_acceleration.y
    lin_acc_z = imu.linear_acceleration.z

    with open(DIRNAME + '/' + FNAME1, 'a') as f:
        writer = csv.writer(f)
        writer.writerow([imu_time, lin_acc_x, lin_acc_y, lin_acc_z, ang_x, \
        ang_y, ang_z, quat_x, quat_y, quat_z, quat_w])
        print("Just added IMU output!")

    #rospy.loginfo(imu_msg)

def log_ekf(ekf):
    '''
    Callback function for /robot_pose_ekf/odom_combined topic
    The csv file format heading is as follows:
    'Timestamp, EKF_position_x, EKF_position_y'
   '''
    print 'Listening to ekf topic...'

    # Collect x & y coordinate values from EKF output
    ekf_msg = PoseWithCovarianceStamped()
    ekf_msg = ekf
    ekf_time = ekf_msg.header.stamp.secs
    ekf_x = ekf.pose.pose.position.x
    ekf_y = ekf.pose.pose.position.y

    with open(DIRNAME + '/' + FNAME2, 'a') as f:
        writer = csv.writer(f)
        writer.writerow([ekf_time, ekf_x, ekf_y])
        print("Just added ekf output!")

    #rospy.loginfo(ekf_msg)

def log_dist(dist):
    '''
    Callback function for vo topic (displacement from linear acceleration)
    The csv file format heading is as follows:
    'Timestamp, displacement_x, displacement_y, velocity_x, velocity_y'
    '''
    print 'Listening to vo topic...'

    # Collect x & y coordinate values from IPS after smoothing buffer

    dist_msg = Odometry()
    dist_msg = dist
    dist_time = dist.header.stamp.secs
    dist_x = dist.pose.pose.position.x
    dist_y = dist.pose.pose.position.y
    vel_x = dist.twist.twist.linear.x
    vel_y = dist.twist.twist.linear.y

    with open(DIRNAME + '/' + FNAME3, 'a') as f:
        writer = csv.writer(f)
        writer.writerow([dist_time, dist_x, dist_y, vel_x, vel_y])
        print("Just added displacement output!")

    #rospy.loginfo(dist_msg)

def log_ips(ips):
    '''
    Callback function for gps topic
    The csv file format heading is as follows:
    'Timestamp, IPS_position_x, IPS_position_y'
    '''
    print 'Listening to gps topic...'

    # Collect x & y coordinate values from IPS after smoothing buffer

    ips_msg = Odometry()
    ips_msg = ips
    ips_time = ips.header.stamp.secs
    ips_x = ips.pose.pose.position.x
    ips_y = ips.pose.pose.position.y

    with open(DIRNAME + '/' + FNAME4, 'a') as f:
        writer = csv.writer(f)
        writer.writerow([ips_time, ips_x, ips_y])
        print("Just added IPS output!")

    #rospy.loginfo(ips_msg)

def log_mag(mag):
    '''
    Callback function for mag_bbb topic
    The csv file format heading is as follows:
    'Timestamp, magnetic_x, magnetic_y, magnetic_z'
    '''
    print 'Listening to mag topic...'

    # Collect x & y coordinate values from mag after smoothing buffer

    mag_msg = MagneticField()
    mag_msg = mag
    mag_time = mag.header.stamp.secs
    mag_x = mag.magnetic_field.x
    mag_y = mag.magnetic_field.y
    mag_z = mag.magnetic_field.z

    with open(DIRNAME + '/' + FNAME5, 'a') as f:
        writer = csv.writer(f)
        writer.writerow([mag_time, mag_x, mag_y, mag_z])
        print("Just added mag output!")

    #rospy.loginfo(mag_msg)

def log_node():

    rospy.init_node('imu_node_error_collect', anonymous=True)
    rospy.Subscriber("imu_bbb", Imu, log_raw_imu)
    rospy.Subscriber("imu_data", Imu, log_imu)
    rospy.Subscriber("/robot_pose_ekf/odom_combined", PoseWithCovarianceStamped, log_ekf)
    rospy.Subscriber("vo", Odometry, log_dist)
    rospy.Subscriber("gps", Odometry, log_ips)
    rospy.Subscriber("mag_bbb", MagneticField, log_mag)

    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()

if __name__ == '__main__':
    log_node()
