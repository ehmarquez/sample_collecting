#!/usr/bin/env python
#import rospy
import time
import numpy as np
import csv

fname1 = "log_imu.csv"
fname2 = "log_mag.csv"

with open(fname1, 'rb') as f:
    reader = csv.reader(f)
    ang_x, ang_y, ang_z = [], [], []
    lin_acc_x, lin_acc_y, lin_acc_z = [], [], []
    mag_x, mag_y, mag_z =  [], [], []
    for row in reader:
        ang_x.append(row[5])
        ang_y.append(row[6])
        ang_z.append(row[7])
        lin_acc_x.append(row[8])
        lin_acc_y.append(row[9])
        lin_acc_z.append(row[10])

with open(fname2, 'rb') as f:
    reader = csv.reader(f)
    for row in reader:
        mag_x.append(row[1])
        mag_y.append(row[2])
        mag_z.append(row[3])

# Convert lists to numpy to calculate numpy  standard deviation with ddof=1
def csv2numpy(val):
    val = np.array(val).astype(np.float)
    #assert type(val[0]) == "<type 'numpy.float64'>", "Must first convert to float before calling np.std!"
    mean = np.mean(val)
    std = np.std(val, ddof=1)
    return (mean, std)

print('Angular Velocity Covariance: \n [ {0}, 0, 0, \n 0, {1}, 0, \n 0, 0, {2}]\n').format(csv2numpy(ang_x)[1], csv2numpy(ang_y)[1], csv2numpy(ang_z)[1])

print('Linear Acceleration Covariance: \n [ {0}, 0, 0, \n 0, {1}, 0, \n 0, 0, {2}]\n').format(csv2numpy(lin_acc_x)[1], csv2numpy(lin_acc_y)[1], csv2numpy(lin_acc_z)[1])

print('Magnetic Field Covariance: \n [ {0}, 0, 0, \n 0, {1}, 0, \n 0, 0, {2}]\n').format(csv2numpy(mag_x)[1], csv2numpy(mag_y)[1], csv2numpy(mag_z)[1])

print 'Mean values:'
print('\n ang_x: {0} \n ang_y: {1} \n ang_z: {2}').format(csv2numpy(ang_x)[0], csv2numpy(ang_y)[0], csv2numpy(ang_z)[0])
print('\n lin_acc_x: {0} \n lin_acc_y: {1} \n lin_acc_z: {2}').format(csv2numpy(lin_acc_x)[0], csv2numpy(lin_acc_y)[0], csv2numpy(lin_acc_z)[0])
print('\n mag_x: {0} \n mag_y: {1} \n mag_z: {2}').format(csv2numpy(mag_x)[0], csv2numpy(mag_y)[0], csv2numpy(mag_z)[0])
