#!/usr/bin/env python

import rospy
import tf
import time
from math import cos, sin, pi, radians, degrees, atan2
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Twist, TransformStamped, Quaternion
from sensor_msgs.msg import MagneticField
from std_msgs.msg import String

# Initialize initial values
x, y, th = 1, 1, 0.1
vx, vy, vth = 0.1, -0.1, 0.1
debug = False

def get_theta(d):
   # print 'Received new values from magnetometer'
    global heading_rad

    if debug is True:
        for i in range(0,360,15):
            heading_deg = i
            heading_rad = radians(heading_deg)
    else:
        heading_deg = degrees(atan2(d.magnetic_field.y, d.magnetic_field.x))
        heading_deg = heading_deg + 180          #need to adjust relative to car position
        if heading_deg < 0:
            heading_deg = heading_deg + 360
        heading_rad = atan2(d.magnetic_field.y, d.magnetic_field.x)
    #print 'value in rad is: ', heading_rad
    #print 'value in deg is: ', heading_deg

def odom_transform(ips_str):
    print 'starting odom_transform...'
    #print 'heading in radians is: ', heading_rad
    th = heading_rad
    current_time = rospy.Time.now()
    odom_quat = tf.transformations.quaternion_from_euler(0, 0, th)              #'rzyx')
    quat_msg = Quaternion(*odom_quat)                               #makes it a quaternion ros msg
    trans_x, trans_y = ips_str.data.split(",")
    trans = (float(trans_x), float(trans_y), 0)                         #z-comp is 0 since 2D
    rotatn = (odom_quat[0], odom_quat[1], odom_quat[2], odom_quat[3])   #makes a tuple quaternion

    #print 'Value of trans is' , trans
    #send the transform between base_link and odom
    br = tf.TransformBroadcaster()
    br.sendTransform((trans), (rotatn), current_time, "base_link", "odom")
    print 'just sent transform, sending odom message...'

    #Publishing the odometry message to /odom
    odom = Odometry()
    odom.header.stamp = current_time
    odom.header.frame_id = "odom"

    #set the position based on IPS
    odom.pose.pose.position.x = float(trans_x)
    odom.pose.pose.position.y = float(trans_y)
    odom.pose.pose.position.z = 0.0
    odom.pose.pose.orientation = quat_msg

    #set the velocity
    odom.child_frame_id = "base_link"
    odom.twist.twist.linear.x = 0 #vx, based on motor speed, find a matching number
    odom.twist.twist.linear.y = 0 #vy
    odom.twist.twist.angular.z = 0.0 #vth, this should match magnetometer and turning speed

    odom_pub.publish(odom)
    print 'Odom message sent to topic /odom!'

if __name__ == '__main__':
    print 'Starting...'
    heading_rad = 0
    rospy.init_node('odometry_publisher')
    odom_pub = rospy.Publisher('odom', Odometry, queue_size = 50)
    current_time = rospy.Time.now()
    last_time = rospy.Time.now()
    #rate = rospy.Rate(1)                    #publish at 5Hz

    #calculate for changes in movement here
    print 'entering while not loop...'
    current_time = rospy.Time.now()

    #Use magnetometer and IPS to update transform between base link and map
    #IPS gives coordinates and magnetometer gives headings in quaternion
    rospy.Subscriber('magnetometer', MagneticField, get_theta)
    rospy.Subscriber('ips_pose', String, odom_transform)

    #Used for DEBUG is True, subscribing to faker.py to call odom_tranform
    if debug is True:
        print 'DEBUG MODE IS ON!'
        rospy.Subscriber('chatter', String, odom_transform)

    last_time = current_time
    rospy.spin()
    print 'Exiting rospy spin loop...'
