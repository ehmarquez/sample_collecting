#!/usr/bin/env python
import rospy
from sensor_msgs.msg import Imu, MagneticField
from geometry_msgs.msg import PoseWithCovarianceStamped
from nav_msgs.msg import Odometry
import csv

# Create csv files and OVERWRITE old files if it already exists!
with open('log_imu.csv', 'wb') as f:
    print 'creating log file for accelerometer...'

with open('log_ekf.csv', 'wb') as f:
    print 'Creating log file for ekf output...'

with open('log_ips.csv', 'wb') as f:
    print 'creating log file for ips output...'

with open('log_mag.csv', 'wb') as f :
    print 'creating log file for mag output...'

def log_imu(imu):

    print 'Listening to imu topic...'

    # Collect values from IMU
    imu_msg = Imu()
    imu_msg = imu
    imu_time = imu_msg.header.stamp

    # Quaternion values (absolute position)
    quat_x = imu.orientation.x
    quat_y = imu.orientation.y
    quat_z = imu.orientation.z
    quat_w = imu.orientation.w

    # Angular velocity
    ang_x = imu.angular_velocity.x
    ang_y = imu.angular_velocity.y
    ang_z = imu.angular_velocity.z

    # Linear Acceleration values
    lin_acc_x = imu.linear_acceleration.x
    lin_acc_y = imu.linear_acceleration.y
    lin_acc_z = imu.linear_acceleration.z

    with open('log_imu.csv', 'a') as f:
        writer = csv.writer(f)
        writer.writerow([imu_time, quat_x, quat_y, quat_z, quat_w, ang_x, ang_y, ang_z, lin_acc_x, lin_acc_y, lin_acc_z])
        print("Just added IMU output!")

    #rospy.loginfo(imu_msg)

def log_ekf(ekf):

    print 'Listening to ekf topic...'

    # Collect x & y coordinate values from EKF output
    ekf_msg = PoseWithCovarianceStamped()
    ekf_msg = ekf
    ekf_time = ekf_msg.header.stamp
    ekf_x = ekf.pose.pose.position.x
    ekf_y = ekf.pose.pose.position.y

    with open('log_ekf.csv', 'a') as f:
        writer = csv.writer(f)
        writer.writerow([ekf_time, ekf_x, ekf_y])
        print("Just added ekf output!")

    #rospy.loginfo(ekf_msg)

def log_ips(ips):

    print 'Listening to ips topic...'

    # Collect x & y coordinate values from IPS after smoothing buffer

    ips_msg = Odometry()
    ips_msg = ips
    ips_time = ips.header.stamp
    ips_x = ips.pose.pose.position.x
    ips_y = ips.pose.pose.position.y

    with open('log_ips.csv', 'a') as f:
        writer = csv.writer(f)
        writer.writerow([ips_time, ips_x, ips_y])
        print("Just added IPS output!")

    #rospy.loginfo(ips_msg)

def log_mag(mag):

    print 'Listening to mag topic...'

    # Collect x & y coordinate values from mag after smoothing buffer

    mag_msg = MagneticField()
    mag_msg = mag
    mag_time = mag.header.stamp
    mag_x = mag.magnetic_field.x
    mag_y = mag.magnetic_field.y
    mag_z = mag.magnetic_field.z

    with open('log_mag.csv', 'a') as f:
        writer = csv.writer(f)
        writer.writerow([mag_time, mag_x, mag_y, mag_z])
        print("Just added mag output!")

    #rospy.loginfo(mag_msg)

def log_node():

    rospy.init_node('imu_node_error_collect', anonymous=True)
    rospy.Subscriber("imu_data", Imu, log_imu)
    rospy.Subscriber("/robot_pose_ekf/odom_combined", PoseWithCovarianceStamped, log_ekf)
    rospy.Subscriber("vo", Odometry, log_ips)
    rospy.Subscriber("mag_bbb", MagneticField, log_mag)

    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()

if __name__ == '__main__':
    log_node()
